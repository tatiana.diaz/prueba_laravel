<?php

namespace Database\Factories;

use App\Models\employee_salary;
use Illuminate\Database\Eloquent\Factories\Factory;

class employee_salaryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = employee_salary::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
