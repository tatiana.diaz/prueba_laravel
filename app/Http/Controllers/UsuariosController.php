<?php

namespace App\Http\Controllers;
use App\Models\Usuarios;
use Illuminate\Http\Request;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = Usuarios::all();

        return $usuarios;
    }

    public function ById(Request $request){
        $usuarios = Usuarios::find($request->id);
        return $usuarios;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
        $usuarios = new usuarios;
    
        $usuarios->nombre = $request->nombre;
        $usuarios->apellido = $request->apellido;
        $usuarios->telefono = $request->telefono;
        $usuarios->correo = $request->correo;
        $usuarios->direccion = $request->direccion;
       
       
        if($usuarios->save()){
            return 'Usuario Creado';
        }else{
            return 'Usuario No Creado';
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function show(usuarios $usuarios)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $id = $request->id;
        
        $usuarios = Usuarios::findOrFail($id);
        
        $usuarios->nombre = $request->nombre;
        $usuarios->apellido = $request->apellido;
        $usuarios->telefono = $request->telefono;
        $usuarios->correo = $request->correo;
        $usuarios->direccion = $request->direccion;
       
        if($usuarios->save()){
            return 'Usuario editado';
        }else{
            return 'Usuario No editado';
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, usuarios $usuarios)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        
        $usuarios = Usuarios::findOrFail($id);
        
        if($usuarios->delete()){
            return 'Usuario eliminado';
        }else{
            return 'Usuario No eliminado';
        }
    }
}
