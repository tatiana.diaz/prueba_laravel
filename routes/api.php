<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/* lista Usuarios */
Route::get("/usuario","App\Http\Controllers\UsuariosController@index");
/* cargos por id */
Route::get("/usuario/{id}","App\Http\Controllers\UsuariosController@ById");
/* crear usuario */
Route::post("/crear_usuario","App\Http\Controllers\UsuariosController@create");
/* editar usuario */
Route::post("/editar_usuario","App\Http\Controllers\UsuariosController@edit");
/* eliminar usuario */
Route::post("/eliminar/{id}","App\Http\Controllers\UsuariosController@destroy");
